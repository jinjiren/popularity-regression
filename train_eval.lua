require 'hdf5'
require 'torch'
require 'cutorch'
require 'cunn'
require 'optim'
require 'xlua'
require 'paths'
require 'unsup'
weight_init = require('weight-init')

-- parse command line arguments
if not opt then
    print('==> processing options')
    cmd = torch.CmdLine()
    cmd:text()
    cmd:text('popularity regression')
    cmd:text()
    cmd:text('Options:')
    local save_path = 'experiments/' .. os.date('%F-%H-%M')
    local model_path = paths.concat(save_path, 'models')
    local data_path = 'train.hdf5'
    cmd:option('-data', data_path, 'data path')
    cmd:option('-save', save_path, 'subdirectory to save/log experiments in')
    cmd:option('-model_save', model_path, 'subdirectory to save models in')
    cmd:option('-plot', false, 'live plot')
    cmd:option('-hidden_nodes', 128, 'number of hidden nodes in each layer')
    cmd:option('-hidden_layer_num', 6, 'number of hidden layers')
    cmd:option('-early_stop', 10, 'early-stopping number')
    cmd:option('-learningRate', 1e-2, 'learning rate at t=0')
    cmd:option('-learningRateDecay', 0, 'learning rate Decay')
    cmd:option('-batchSize', 1, 'mini-batch size (1 = pure stochastic)')
    cmd:option('-weightDecay', 0, 'weight decay (SGD only)')
    cmd:option('-momentum', 0, 'momentum (SGD only)')
    cmd:option('-dropout', 0.5, 'dropout')
    cmd:text()
    opt = cmd:parse(arg)
    print(opt)

    --create log file
    paths.mkdir(opt.save)
    paths.mkdir(opt.model_save)
    cmd:log(paths.concat(opt.save,'log'),params)
end


-- log results to files
local trainLogger = optim.Logger(paths.concat(opt.save, 'train.log'))
local valLogger = optim.Logger(paths.concat(opt.save, 'val.log'))

torch.setdefaulttensortype('torch.FloatTensor')
torch.manualSeed(1)
local myFile = hdf5.open(opt.data, 'r')
local train_x = myFile:read('train_x'):all():float()
train_x = unsup.zca_whiten(train_x)
train_x = train_x:cuda()
local train_y = myFile:read('train_y'):all():cuda()
local val_x = myFile:read('val_x'):all():float()
val_x = unsup.zca_whiten(val_x)
val_x = val_x:cuda()
local val_y = myFile:read('val_y'):all():cuda()
--local val_x = myFile:read('val_x'):all():cuda()
--local val_y = myFile:read('val_y'):all():t():cuda()
local test_x = myFile:read('test_x'):all():float()
test_x = unsup.zca_whiten(test_x)
test_x = test_x:cuda()
myFile:close()
collectgarbage()

print('train set size: ' .. train_x:size(1))
print('validation set size: ' .. val_x:size(1))
print('test set size: ' .. test_x:size(1))

-- define deep neural networks
local hidden_nodes = opt.hidden_nodes
local model = nn.Sequential()
model:add(nn.Linear(train_x:size(2), hidden_nodes))
model:add(nn.ReLU())
hidden_layer_num = opt.hidden_layer_num
for i = 1,hidden_layer_num do
    model:add(nn.Dropout(opt.dropout))
    model:add(nn.Linear(hidden_nodes, hidden_nodes))
    model:add(nn.ReLU())
end
model:add(nn.Dropout(opt.dropout))
model:add(nn.Linear(hidden_nodes, 1))
-- initialize weights
--local method = 'xavier'
--local model = weight_init(model, method)
model = model:cuda()
local criterion = nn.AbsCriterion()
criterion = criterion:cuda()
local w, dw = model:getParameters()

-- verbose
print('<polularity regression> using model:')
print(model)

-- set variables for early-stopping
local train_e = {}
local val_e = {}
local overfit_count = {}

--[[-- evaluate function for optim
local function feval(new_w)
    if w~=new_w then
        w:copy(new_w)
    end
    -- iterately choose training sample
    index = (index or 0) + 1
    if index > train_x:size(1) then index = 1 end
    local input = train_x[index]
    local target = train_y[index]
    -- initialize grad parameters
    dw:zero()
    local prediction = model:forward(input)
    local loss = criterion:forward(prediction, target)
    -- backpropagation
    model:backward(input, criterion:backward(prediction, target))
    return loss, dw
end--]]

-- hyperparameters for learning
local learning_params = {
   learningRate = opt.learningRate,
   learningRateDecay = opt.learningRateDecay,
   weightDecay = opt.weightDecay,
   momentum = 0.9
}

function train(epoch)
    -- set model to training mode
    -- model:training()
    -- epoch training start time
    local time = sys.clock()
    -- shuffle at each epoch
    shuffle = torch.randperm(train_x:size(1))
    -- do one epoch
    print('\n<trainer> on training set:')
    print("<trainer> online epoch # " .. epoch .. ' [batchSize = ' .. opt.batchSize .. ']')
    local total_loss = 0
    for t = 1,train_x:size(1),opt.batchSize do
        -- create mini batch
        local inputs = torch.CudaTensor(opt.batchSize, train_x:size(2))
        local targets = torch.CudaTensor(opt.batchSize, train_y:size(2))
        local k = 1
        for i = t,math.min(t+opt.batchSize-1,train_x:size(1)) do
            -- load new sample
            local input = train_x[shuffle[i]]
            local target = train_y[shuffle[i]]
            inputs[k] = input
            targets[k] = target
            k = k + 1
        end
        -- define evaluate function for optim
        local function feval(new_w)
            if w~=new_w then
                w:copy(new_w)
            end
     
            local average_loss = 0
            -- reset gradients
            dw:zero()
            
            -- iterately choose training sample
            for i = 1,inputs:size(1) do
                local prediction = model:forward(inputs[i])
                local loss = criterion:forward(prediction, targets[i])
                average_loss = average_loss + loss
                total_loss = total_loss + loss 
                model:backward(inputs[i], criterion:backward(prediction, targets[i]))
            end
            dw:div(inputs:size(1))
            average_loss = average_loss/inputs:size(1)
            
            return average_loss, dw
        end
        
        _, fs = optim.adagrad(feval, w, learning_params)
    end
   
    -- time taken
    time = sys.clock() - time
    time = time / train_x:size(1)
    print("==> time to learn 1 sample = " .. (time*1000) .. 'ms')
    local train_err = total_loss/train_x:size(1)
    print('train_err: ' .. train_err)
    trainLogger:add{['% mean abs loss (train set)'] = train_err}
    if opt.plot then
        trainLogger:style{['% mean abs loss (train set)'] = '-'}
        trainLogger:plot()
    end
    
    -- save model
    torch.save(opt.model_save .. '/model-' .. epoch .. '.t7', model)
    return train_err
end
    
function val()
    local current_loss = 0
    for i=1, val_x:size(1) do
        pred = model:forward(val_x[i])
        loss = criterion:forward(pred, val_y[i])
        current_loss = current_loss + loss
    end
    val_err = current_loss/val_x:size(1)
    print('val_err: ' .. val_err)
    --update log/plot
    valLogger:add{['% mean abs loss (validation set)'] = val_err}
    if opt.plot then
        valLogger:style{['% mean abs loss (validation set)'] = '-'}
        valLogger:plot()
    end
    return val_err
end

function test()
    model:evaluate()
    local f = torch.DiskFile(opt.save .. '/result.txt', 'w')
    for i=1, test_x:size(1) do
        local pred = model:forward(test_x[i]):float()
        f:writeFloat(pred[1])
    end
    f:close()
end
    


for epoch = 1,500 do
    if epoch % 10 == 0 then
        learning_params['learningRate'] = learning_params['learningRate'] / 1.25
        print('Learning rate changes to '.. learning_params['learningRate'])
    end
    model:training()
    train_e[epoch] = train(epoch)
    model:evaluate()
    val_e[epoch] = val()
    -- early-stopping: if val_e increases when train_e decreases for continued 5 times, stop training
    overfit_count[epoch] = 0
    if epoch > opt.early_stop then
        --if (train_e[epoch] < train_e[epoch - 1]) and (val_e[epoch] > val_e[epoch - 1]) then
        if (val_e[epoch] > val_e[epoch - 1]) then
            overfit_count[epoch] = 1
            count = 0
            for j=0,opt.early_stop-1 do
                count = count + overfit_count[epoch - j]
            end
            if count == opt.early_stop then 
                break
            end
        end
    end
end

local best_val_err, best_val_model = torch.min(torch.Tensor(val_e),1)
print('minimum validation err: ' .. best_val_err[1])
print('best model on validation set: ' .. best_val_model[1])
local best_train_err, best_train_model = torch.min(torch.Tensor(train_e), 1)
print('minimum train err: ' .. best_train_err[1])
print('best model on train set: ' .. best_train_model[1])

-- choose the best model for validation to test
local model = torch.load(opt.model_save .. '/model-' .. best_val_model[1] .. '.t7')
model:evaluate()
test()
