import numpy as np
import pandas as pd
import regression as reg
import h5py

N = 28000
N_val = 20000

train_data = pd.read_csv("train.csv")
test_data = pd.read_csv("test.csv")

train_data = train_data.drop(['url'], axis=1) #remove 'url' information.
train_data = train_data.drop(['timedelta'], axis=1) #remove 'url' information.
X = np.matrix(train_data.drop(['shares'], axis=1))
y = np.matrix(train_data['shares']) #This is the target

XTrain = X[:N,:] #use the first N samples for training
yTrain = y[:,:N]
XVal = X[N_val:,:] #use the rests for validation
yVal = y[:,N_val:]

with h5py.File("train_28000.hdf5", "w") as f:
    f.create_dataset("train_x", data=XTrain)
    f.create_dataset("train_y", data=yTrain)
    f.create_dataset("val_x", data=XVal)
    f.create_dataset("val_y", data=yVal)
    f.create_dataset("test_x", data=test_data)


w = reg.standRegres(XTrain,yTrain) #linear regression

yHatTrain = np.dot(XTrain,w)
yHatVal = np.dot(XVal,w)

print "Training error ", np.mean(np.abs(yTrain - yHatTrain.T))
print "Validation error ", np.mean(np.abs(yVal - yHatVal.T))

yHatTest = np.dot(np.matrix(test_data),w)
np.savetxt('result.txt', yHatTest)
